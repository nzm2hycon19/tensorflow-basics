# tensorflow basics
**Local installation** <br>
Copy this repository to your computer 
```bash
#clone repository
git clone https://gitlab.com/nzm2hycon19/tensorflow-basics.git
cd tensorflow-basics
```
update version 
``` bash
git pull 
```

Create environment with Anaconda
```bash
conda env create -n tensorflow_gpuenv environment.yml
#activate environment
conda activate tensorflow_gpuenv
```

Start jupyter notebook
```bash
jupyter notebook
```